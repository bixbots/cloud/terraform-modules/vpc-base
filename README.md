# vpc-base

## Summary

Provides a base module used to deploy VPCs in AWS. This module does not deploy any NAT resources and routing from non-public subnets requires additional configuration.

## Resources

This module creates the following resources:

* [aws_vpc](https://www.terraform.io/docs/providers/aws/r/vpc.html)
* [aws_vpc_endpoint](https://www.terraform.io/docs/providers/aws/r/vpc_endpoint.html)
* [aws_internet_gateway](https://www.terraform.io/docs/providers/aws/r/internet_gateway.html)
* [aws_subnet](https://www.terraform.io/docs/providers/aws/r/subnet.html)
* [aws_db_subnet_group](https://www.terraform.io/docs/providers/aws/r/db_subnet_group.html)
* [aws_elasticache_subnet_group](https://www.terraform.io/docs/providers/aws/r/elasticache_subnet_group.html)
* [aws_redshift_subnet_group](https://www.terraform.io/docs/providers/aws/r/redshift_subnet_group.html)
* [aws_default_security_group](https://www.terraform.io/docs/providers/aws/r/default_security_group.html)
* [aws_default_route_table](https://www.terraform.io/docs/providers/aws/r/default_route_table.html)
* [aws_route_table](https://www.terraform.io/docs/providers/aws/r/route_table.html)
* [aws_route_table_association](https://www.terraform.io/docs/providers/aws/r/route_table_association.html)
* [aws_route](https://www.terraform.io/docs/providers/aws/r/route.html)

## Cost

Use of this module will result in billable resources being launched in AWS. Before using this module, please familiarize yourself with the expenses associated with VPCs.

* AWS VPC pricing information is available [here](https://aws.amazon.com/vpc/pricing/)

## Inputs

| Name                    | Description                                                                                                                                                                          | Type           | Default   | Required |
|:------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:----------|:---------|
| application             | Name of the application the resource(s) will be a part of.                                                                                                                           | `string`       | `core`    | no       |
| environment             | Name of the environment the resource(s) will be a part of. This will be the simple name (i.e. short name) of the VPC.                                                                | `string`       | -         | yes      |
| role                    | Name of the role the resource(s) will be performing.                                                                                                                                 | `string`       | `network` | no       |
| zone_id                 | Specifies the Route53 Zone ID that this VPC belongs to.                                                                                                                              | `string`       | -         | yes      |
| cidr                    | CIDR of VPC. All subnets must be inside of this CIDR.                                                                                                                                | `string`       | -         | yes      |
| public_subnets          | A list of public subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc).  | `list(string)` | -         | yes      |
| private_subnets         | A list of private subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc). | `list(string)` | -         | yes      |
| data_subnets            | A list of data subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc).    | `list(string)` | -         | yes      |
| azs                     | A list of availability zones in the region.                                                                                                                                          | `list(string)` | -         | yes      |
| enable_dns_hostnames    | Specifies whether to enable DNS hostnames in the VPC.                                                                                                                                | `bool`         | `true`    | no       |
| enable_dns_support      | Specifies whether to enable DNS support in the VPC.                                                                                                                                  | `bool`         | `true`    | no       |
| map_public_ip_on_launch | Specifies whether to auto-assign public IPs when launching instances in public subnets.                                                                                              | `bool`         | `true`    | no       |
| tags                    | Additional tags to attach to all resources created by this module.                                                                                                                   | `map(string)`  | -         | no       |

## Outputs

| Name                      | Description                                                                   |
|:--------------------------|:------------------------------------------------------------------------------|
| vpc_id                    | The ID of the VPC.                                                            |
| default_security_group_id | The ID of the default security group.                                         |
| gateway_id                | The ID of the internet gateway.                                               |
| public_subnets            | A list containing the ID of public subnets.                                   |
| private_subnets           | A list containing the ID of private subnets.                                  |
| data_subnets              | A list containing the ID of data subnets.                                     |
| public_route_table_id     | The ID of the public routing table.                                           |
| private_route_table_ids   | A list containing the ID of private routing tables.                           |
| data_route_table_ids      | A list containing the ID of data routing tables.                              |
| tags                      | A combined list of tags including input tags and tags created by this module. |

## Examples

### Simple Usage

```terraform
module "vpc_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/vpc-base.git?ref=v1"

  application = "core"
  environment = "example"
  role        = "network"

  zone_id         = "todo"
  cidr            = "10.144.0.0/16"
  public_subnets  = ["10.144.0.0/21", "10.144.64.0/21", "10.144.128.0/21"]
  private_subnets = ["10.144.16.0/20", "10.144.80.0/20", "10.144.144.0/20"]
  data_subnets    = ["10.144.8.0/21", "10.144.72.0/21", "10.144.136.0/21"]
  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]

  tags = {
    Application  = "core"
    Environment  = "example"
    Role         = "network"
    Lifespan     = "permanent"
  }
}
```

## Version History

* v1 - Initial Release
