locals {
  name_prefix = "${var.application}-${var.environment}-${var.role}"
  common_tags = merge(var.tags, {
    "VPC"  = var.environment
    "Role" = var.role
  })
}

resource "aws_vpc" "primary" {
  cidr_block           = var.cidr
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = merge(local.common_tags, {
    "Name"    = var.environment
    "DNSZone" = trimsuffix(data.aws_route53_zone.primary.name, ".")
  })
}

resource "aws_internet_gateway" "primary" {
  vpc_id = aws_vpc.primary.id

  tags = merge(local.common_tags, {
    "Name" = local.name_prefix
  })
}

resource "aws_default_security_group" "primary" {
  vpc_id = aws_vpc.primary.id

  tags = merge(local.common_tags, {
    "Name" = "${local.name_prefix}-default"
  })
}

resource "aws_default_route_table" "primary" {
  default_route_table_id = aws_vpc.primary.default_route_table_id

  tags = merge(local.common_tags, {
    "Name" = local.name_prefix
  })
}
