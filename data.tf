data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_route53_zone" "primary" {
  zone_id = var.zone_id
}

data "aws_availability_zone" "primary" {
  count = length(var.azs)
  name  = var.azs[count.index]
}
