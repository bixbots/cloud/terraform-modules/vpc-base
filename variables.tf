variable "application" {
  type        = string
  default     = "core"
  description = "Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of. This will be the simple name (i.e. short name) of the VPC."
}

variable "role" {
  type        = string
  default     = "network"
  description = "Name of the role the resource(s) will be performing."
}

variable "zone_id" {
  type        = string
  description = "Specifies the Route53 Zone ID that this VPC belongs to."
}

variable "cidr" {
  type        = string
  description = "CIDR of VPC. All subnets must be inside of this CIDR."
}

variable "public_subnets" {
  type        = list(string)
  description = "A list of public subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc)."
}

variable "private_subnets" {
  type        = list(string)
  description = "A list of private subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc)."
}

variable "data_subnets" {
  type        = list(string)
  description = "A list of data subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc)."
}

variable "azs" {
  type        = list(string)
  description = "A list of availability zones in the region."
}

variable "enable_dns_hostnames" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Specifies whether to enable DNS hostnames in the VPC."
}

variable "enable_dns_support" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Specifies whether to enable DNS support in the VPC."
}

variable "map_public_ip_on_launch" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Specifies whether to auto-assign public IPs when launching instances in public subnets."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
