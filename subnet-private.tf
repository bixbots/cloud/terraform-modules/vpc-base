resource "aws_subnet" "private" {
  count             = length(var.azs)
  vpc_id            = aws_vpc.primary.id
  cidr_block        = var.private_subnets[count.index]
  availability_zone = var.azs[count.index]

  tags = merge(local.common_tags, {
    "Name"                   = "${local.name_prefix}-private-${var.azs[count.index]}"
    "Type"                   = "private"
    "AvailabilityZone"       = var.azs[count.index]
    "AvailabilityZoneSuffix" = data.aws_availability_zone.primary[count.index].name_suffix
  })
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.primary.id
  count  = length(var.private_subnets)

  tags = merge(local.common_tags, {
    "Name"             = "${local.name_prefix}-private-${var.azs[count.index]}"
    "Type"             = "private"
    "AvailabilityZone" = var.azs[count.index]
  })
}

resource "aws_route_table_association" "private" {
  count          = length(var.private_subnets)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}
