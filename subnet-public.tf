resource "aws_subnet" "public" {
  count                   = length(var.azs)
  vpc_id                  = aws_vpc.primary.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = var.azs[count.index]
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = merge(local.common_tags, {
    "Name"                   = "${local.name_prefix}-public-${var.azs[count.index]}"
    "Type"                   = "public"
    "AvailabilityZone"       = var.azs[count.index]
    "AvailabilityZoneSuffix" = data.aws_availability_zone.primary[count.index].name_suffix
  })
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.primary.id

  tags = merge(local.common_tags, {
    "Name" = "${local.name_prefix}-public"
    "Type" = "public"
  })
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_subnets)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

resource "aws_route" "public" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.primary.id
  route_table_id         = aws_route_table.public.id
}
