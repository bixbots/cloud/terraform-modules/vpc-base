output "name_prefix" {
  description = "The name prefix of the VPC."
  value       = local.name_prefix
}

output "vpc_id" {
  description = "The ID of the VPC."
  value       = aws_vpc.primary.id
}

output "default_security_group_id" {
  description = "The ID of the default security group."
  value       = aws_vpc.primary.default_security_group_id
}

output "gateway_id" {
  description = "The ID of the internet gateway."
  value       = aws_internet_gateway.primary.id
}

output "public_subnets" {
  description = "A list containing the ID of public subnets."
  value       = aws_subnet.public.*.id
}

output "private_subnets" {
  description = "A list containing the ID of private subnets."
  value       = aws_subnet.private.*.id
}

output "data_subnets" {
  description = "A list containing the ID of data subnets."
  value       = aws_subnet.data.*.id
}

output "public_route_table_id" {
  description = "The ID of the public routing table."
  value       = aws_route_table.public.id
}

output "private_route_table_ids" {
  description = "A list containing the ID of private routing tables."
  value       = aws_route_table.private.*.id
}

output "data_route_table_ids" {
  description = "A list containing the ID of data routing tables."
  value       = aws_route_table.data.*.id
}

output "tags" {
  description = "A combined list of tags including input tags and tags created by this module."
  value       = local.common_tags
}
