resource "aws_subnet" "data" {
  count             = length(var.azs)
  vpc_id            = aws_vpc.primary.id
  cidr_block        = var.data_subnets[count.index]
  availability_zone = var.azs[count.index]

  tags = merge(local.common_tags, {
    "Name"                   = "${local.name_prefix}-data-${var.azs[count.index]}"
    "Type"                   = "data"
    "AvailabilityZone"       = var.azs[count.index]
    "AvailabilityZoneSuffix" = data.aws_availability_zone.primary[count.index].name_suffix
  })
}

resource "aws_db_subnet_group" "data" {
  name       = local.name_prefix
  subnet_ids = aws_subnet.data.*.id
}

resource "aws_elasticache_subnet_group" "data" {
  name       = local.name_prefix
  subnet_ids = aws_subnet.data.*.id
}

resource "aws_redshift_subnet_group" "data" {
  name       = local.name_prefix
  subnet_ids = aws_subnet.data.*.id
}

resource "aws_route_table" "data" {
  vpc_id = aws_vpc.primary.id
  count  = length(var.data_subnets)

  tags = merge(local.common_tags, {
    "Name"             = "${local.name_prefix}-data-${var.azs[count.index]}"
    "Type"             = "data"
    "AvailabilityZone" = var.azs[count.index]
  })
}

resource "aws_route_table_association" "data" {
  count          = length(var.data_subnets)
  subnet_id      = aws_subnet.data[count.index].id
  route_table_id = aws_route_table.data[count.index].id
}
